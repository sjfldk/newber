import React, { AppRegistry, View, StyleSheet, Text } from 'react-native';
import 'es6-symbol/implement';

import Welcome from './src/containers/Welcome';
import MapExample from './src/containers/Map';
import Root from './src/root';

AppRegistry.registerComponent('newber', () => Root);