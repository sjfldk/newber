'use strict';

import React, {
  StyleSheet,
  View,
  Text,
  InteractionManager,
  ProgressBarAndroid,
  ViewPagerAndroid,
  ScrollView,
  Dimensions
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux/native';
import Mapbox from 'react-native-mapbox-gl';
import MapboxClient from '../util/mapbox';
import {
  Button,
  Card,
  Subheader,
  Divider
} from 'react-native-material-design';
import _ from 'lodash';

import { actions } from '../reducer';

const ACCESS_TOKEN = 'pk.eyJ1Ijoic2pmbGRrIiwiYSI6ImNpa3kxZzA2YzAwNGJ3N20wbTJ2MTdrY3MifQ.LBvXlSNXP4U9maTu8azbwQ';
const mapRef = 'map';

const Map = React.createClass({
  mixins: [Mapbox.Mixin],
  getInitialState() {
    return {
      renderPlaceholderOnly: true,
      annotations: [],
      debug: "moo",
      mapbox: new MapboxClient(ACCESS_TOKEN),
      initialPosition: 'unknown',
      lastPosition: 'unknown'
    };
  },
  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ renderPlaceholderOnly: false });
    });

    navigator.geolocation.getCurrentPosition(
      position => {
        this.props.setLocation(position.coords);
        this.setCenterCoordinateAnimated(mapRef, position.coords.latitude, position.coords.longitude);
      },
      (error) => alert(error.message),
      { enableHighAccuracy: true, timeout: 5000, maximumAge: 1000 }
    );
    this.watchID = navigator.geolocation.watchPosition((position) => {
      var lastPosition = JSON.stringify(position);
      this.setState({ lastPosition });
    });
  },

  componentWillReceiveProps(nextProps) {
    console.log("directions", nextProps.directions);
    this.addAnnotations(mapRef, this.getAnnotations(nextProps));
    if (nextProps.directions.areValid) {
      const steps = nextProps.directions.steps;
      const instructions = steps.map(step =>
        step.maneuver.instruction
      );
      this.setState({
        steps: this.state.steps.cloneWithPages(instructions)
      });

      this.newCenter(nextProps.directions);
    }

  },

  getAnnotations(props) {
    const annotations = [];
    if (props.directions.areValid) {
      annotations.push({
        type: "polyline",
        strokeColor: '#00FB00',
        strokeWidth: 3,
        alpha: 0.5,
        coordinates: props.directions.coordinates
      });
    }

    Object.keys(props.riders).forEach(id => {
      const rider = props.riders[id];
      annotations.push({
        type: 'point',
        title: id,
        coordinates: [rider.location.latitude, rider.location.longitude]
      });
    });

    return annotations;
  },
  onUserLocationChange(location) {
    console.log(location);
  },
  onLongPress(location) {
    console.log(location.src.latitude);
  },
  onOpenAnnotation(annotation) {
    this.props.setRider(annotation.src.title);
  },
  renderPlaceholderView() {
    return (
      <View >
        <Text>Loading...</Text>
      </View >
    );
  },
  renderPage(data, index) {
    return (
      <View
        style={styles.pageStyle}
        key={index}
        >
        <Text>{data}</Text>
      </View>
    );
  },
  newCenter(directions) {
    if (!directions.areValid) return;
    const step = directions.steps[directions.currentStep];
    const currentCoords = step.maneuver.location.coordinates;
    const x = currentCoords[1], y = currentCoords[0];
    this.setVisibleCoordinateBoundsAnimated(
      mapRef, x - 0.01, y - 0.01, x + 0.01, y + 0.01, 0, 0, 0, 0);
  },
  render() {
    if (this.state.renderPlaceholderOnly) {
      return this.renderPlaceholderView();
    }

    const {
      directions,
      findingRiders,
      rider,
      riders,
      setStep,
      findRiders,
      location
    } = this.props;

    return (
      <View style={styles.container}>

        <ScrollView style={styles.debug}>
          <Button
            text="find riders"
            onPress={() => findRiders(1000) }
            />
          <Text>You have selected {
            rider ? riders[rider].name : 'no one!'
          }</Text>
        </ScrollView>
        <Mapbox
          ref={mapRef}
          annotations={this.state.annotations}
          accessToken={ACCESS_TOKEN}
          debugActive={false}
          direction={10}
          onRegionChange={this.onRegionChange}
          scrollEnabled={true}
          showsUserLocation={true}
          zoomLevel={13}
          onUserLocationChange={this.onUserLocationChange}
          onLongPress={this.onLongPress}
          onOpenAnnotation={this.onOpenAnnotation}
          style={styles.container}
          />
        <ViewPagerAndroid
          style={styles.pager}
          onPageSelected={event => setStep(event.nativeEvent.position) }
          >
          { _.map(directions.steps, (step, index) =>
            this.renderPage(step.maneuver.instruction, index)) }
        </ViewPagerAndroid>
        <ProgressBarAndroid
          progress={0}
          indeterminate={directions.pending || findingRiders}
          styleAttr='Horizontal'
          />
      </View >
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 5
  },
  pager: {
    flex: 1
  },
  debug: {
    flex: 1
  },
  pageStyle: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: 30
  }
});

const mapStateToProps = state => ({
  user: state.user,
  riders: state.riders,
  rider: state.rider,
  directions: state.directions,
  findingRiders: state.findingRiders,
  location: state.location
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Map);
