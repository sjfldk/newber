'use strict';

import React, {
  StyleSheet,
  Text,
  View,
  ProgressBarAndroid,
  ListView
} from 'react-native';

import {
  Button,
  Card,
  Subheader,
  Divider
} from 'react-native-material-design';

export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows(
        ['row 1', 'row 2', 'row 3', 'row 4', 'row 5'])
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <Subheader text="Summary"/>

        <Card>
          <Card.Body style={styles.stats}>
            <View style={styles.stat}>
              <Text style={styles.title}>Earnings</Text>
              <Text style={styles.value}>£230.40</Text>
            </View>
            <Divider/>
            <View style={styles.stat}>
              <Text style={styles.title}>Miles</Text>
              <Text style={styles.value}>232</Text>
            </View>
            <Divider/>
          </Card.Body>
        </Card>
        <Divider />
        <Subheader text="History"/>

        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
            <Card>
              <Card.Body>
                <Text>{rowData}</Text>
              </Card.Body>
            </Card>}
          />
        <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
          />
        <Button
          onPress={() => this.props.navigator.push({ id: 'Map' }) }
          raised={true}
          text="Go online"
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewPager: {
    flex: 1
  },
  pageStyle: {
    alignItems: 'center',
    padding: 20
  },
  container: {
    flexDirection: 'column',
    flex: 1
  },
  main: {
    flex: 1
  },
  summary: {

  },
  stats: {

    flexDirection: 'row'
  },
  stat: {
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  title: {
    fontStyle: 'italic'
  },
  value: {
    flex: 1,
    fontWeight: 'bold',
    fontSize: 30
  }
});


// var stylesOld = StyleSheet.create({
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
//   baseText: {
//     fontFamily: 'Cochin',
//   },
//   buttons: {
//     flexDirection: 'row',
//     height: 30,
//     backgroundColor: 'black',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//   },
//   button: {
//     flex: 1,
//     width: 0,
//     margin: 5,
//     borderColor: 'gray',
//     borderWidth: 1,
//     backgroundColor: 'gray',
//   },
//   buttonDisabled: {
//     backgroundColor: 'black',
//     opacity: 0.5,
//   },
//   buttonText: {
//     color: 'white',
//   },
//   scrollStateText: {
//     color: '#99d1b7',
//   },
//   container: {
//     flex: 1,
//     flexDirection: 'column',

//     backgroundColor: 'white',
//   },
//   image: {
//     width: 300,
//     height: 200,
//     padding: 20,
//   },
//   likeButton: {
//     backgroundColor: 'rgba(0, 0, 0, 0.1)',
//     borderColor: '#333333',
//     borderWidth: 1,
//     borderRadius: 5,
//     flex: 1,
//     margin: 8,
//     padding: 8,
//   },
//   likeContainer: {
//     flexDirection: 'row',
//   },
//   likesText: {
//     flex: 1,
//     fontSize: 18,
//     alignSelf: 'center',
//   },
//   progressBarContainer: {
//     height: 10,
//     margin: 10,
//     borderColor: '#eeeeee',
//     borderWidth: 2,
//   },
//   progressBar: {
//     alignSelf: 'flex-start',
//     flex: 1,
//     backgroundColor: '#eeeeee',
//   },
//   viewPager: {
//     flex: 1,
//   },
// });