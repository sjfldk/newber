import Immutable from 'seamless-immutable';
export const
  ADD_RIDER = 'ADD_RIDER',
  SET_RIDER = 'SET_RIDER',
  GET_DIRECTIONS = 'GET_DIRECTIONS',
  GOT_DIRECTIONS = 'GOT_DIRECTIONS',
  SET_STEP = 'SET_STEP',
  FIND_RIDERS = 'FIND_RIDERS',
  FOUND_RIDER = 'FOUND_RIDER',
  SET_LOCATION = 'SET_LOCATION'
  ;

export default function reducer(state, action) {
  switch (action.type) {
    case ADD_RIDER:
      return state.setIn(['riders', action.id], action.details);
    case SET_RIDER:
      return state.set('rider', action.id);
    case GET_DIRECTIONS:
      return state.setIn(['directions', 'pending'], true);
    case GOT_DIRECTIONS:
      return state
        .setIn(['directions', 'pending'], false)
        .setIn(['directions', 'areValid'], action.areValid)
        .setIn(['directions', 'coordinates'], action.coordinates)
        .setIn(['directions', 'steps'], action.steps);
    case SET_STEP:
      return state.setIn(['directions', 'currentStep'], action.step);
    case FIND_RIDERS:
      return state.set('findingRiders', true);
    case FOUND_RIDER:
      return state.setIn(['riders', action.id], action.rider);
    case SET_LOCATION:
      return action.error ? state
        .setIn(['location', 'error'], action.error)
        .setIn(['location', 'valid'], false)
      : state
        .set('location', action.location)
        .setIn(['location', 'valid'], true);
    default:
      return state;
  }
}

export const actions = {
  addRider: (id, details) => ({ // TODO: choose either this or foundRider
    type: ADD_RIDER,
    id, details
  }),
  setRider: id => ({
    type: SET_RIDER,
    id
  }),
  getDirections: destination => ({
    type: GET_DIRECTIONS,
    destination
  }),
  gotDirections: (areValid, coordinates, steps) => ({
    type: GOT_DIRECTIONS,
    areValid,
    coordinates,
    steps
  }),
  setStep: step => ({
    type: SET_STEP,
    step
  }),
  findRiders: radius => ({
    type: FIND_RIDERS,
    radius
  }),
  foundRider: (id, rider) => ({
    type: FOUND_RIDER,
    id,
    rider
  }),
  setLocation: location => ({
    type: SET_LOCATION,
    location
  })
};