import React, {
  Navigator,
  BackAndroid
} from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { Provider} from 'react-redux/native';
import Immutable from 'seamless-immutable';

import configureStore from './store/configureStore';
import Welcome from './containers/Welcome';
import Map from './containers/Map';
import Geolocation from './containers/Geolocation';

const initialState = Immutable({
  user: {
    name: null
  },
  history: {

  },
  location: {
    valid: false
  },
  rider: null,
  findingRiders: false,
  riders: {

  },
  directions: {
    areValid: false,
    pending: false,
    coordinates: null,
    steps: null,
    currentStep: 0
  }
});

const store = configureStore(initialState);

let _navigator;
BackAndroid.addEventListener('hardwareBackPress', () => {
  if (_navigator.getCurrentRoutes().length === 1) {
    return false;
  }
  _navigator.pop();
  return true;
});

export default class Root extends React.Component {
  renderScene(route, navigator) {
    _navigator = navigator;
    switch (route.id) {
      case 'Welcome':
        return <Welcome navigator={navigator} />;
      case 'Map':
        return <Map navigator={navigator} />;
      default:
        return <Welcome navigator={navigator} />;
    }
  }
  render() {
    return (
      <Provider store={store}>
        {() =>
          <Navigator
            ref="navigator"
            initialRoute={{ id: 'Map' }}
            renderScene={this.renderScene}
            configureScene={() => Navigator.SceneConfigs.PushFromRight}
            />
        }
      </Provider>
    );
  }
}