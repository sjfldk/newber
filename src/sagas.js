// import meow, { take, call, put } from 'redux-saga';
var saga = require('redux-saga');
const { take, call, put, select } = saga.effects;
import Chance from 'chance';
const chance = new Chance();

import MapboxClient from './util/mapbox';
import { actions, GET_DIRECTIONS, FIND_RIDERS } from './reducer';
import randomGeo from './util/randomGeo';

const ACCESS_TOKEN = 'pk.eyJ1Ijoic2pmbGRrIiwiYSI6ImNpa3kxZzA2YzAwNGJ3N20wbTJ2MTdrY3MifQ.LBvXlSNXP4U9maTu8azbwQ';
const mapbox = new MapboxClient(ACCESS_TOKEN);
const MAX_SEARCH_TIME = 10000;
export const delay = ms =>
  new Promise(resolve => setTimeout(resolve, Math.random() * ms));

export function* directions() {
  while (true) {
    const { destination } = yield take(GET_DIRECTIONS);
    console.log("destination", destination);
    const userLocation = yield select(state => state.location);
    const result = yield call(mapbox.getDirections.bind(mapbox),
      [userLocation, destination],
      { alternatives: false }
    );
    console.log("result", result);
    const coordinates = result.routes[0].geometry.coordinates;
    const steps = result.routes[0].steps;
    yield put(actions.gotDirections(true, coordinates, steps));
  }
}

export function* riders(getState) {
  while (true) {
    const { radius } = yield take(FIND_RIDERS);
    for (let i = 0; i < 5; i++) {
      yield call(delay, MAX_SEARCH_TIME);
      const userLocation = yield select(state => state.location);
      const riderLocation = randomGeo(userLocation, radius);
      yield put(actions.foundRider(chance.guid(), {
        name: chance.name(),
        location: riderLocation
      }));
    }
  }
}