import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import devTools from 'remote-redux-devtools';
import reducer from '../reducer';
import { directions, riders } from '../sagas';

export default function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(createSagaMiddleware(directions, riders)),
    devTools()
  );
  // Note: passing enhancer as last argument requires redux@>=3.1.0
  return createStore(reducer, initialState, enhancer);
}