const BASE_URL = "https://api.mapbox.com/v4/directions/",
  DEFAULT_PROFILE = "mapbox.driving";

export default class MapboxClient {
  constructor(accessToken) {
    this.accessToken = accessToken;
  }

  static toWaypoints(waypoints) {
    let waypointString = "";
    waypoints.forEach((coord, index) => {
      const joined = coord.longitude + "," + coord.latitude;
      waypointString += joined + (index < waypoints.length - 1 ? ";" : "");
    });
    return waypointString;
  }

  static toParams(params, condition) {
    let paramString = "?";
    const keys = Object.keys(params).filter(condition);
    keys.forEach((param, index) => {
      const joined = param + "=" + params[param];
      paramString += joined + (index < keys.length - 1 ? '&' : '');
    });
    return paramString;
  }

  getDirections(waypoints, options) {
    if (!options.profile) options.profile = DEFAULT_PROFILE;
    options.access_token = this.accessToken;

    const url = BASE_URL + options.profile + "/" + MapboxClient.toWaypoints(waypoints) +
      ".json" + MapboxClient.toParams(options, op => op !== "profile");
    console.log(url);
    return fetch(url)
      .then(response => {
        let json = response.json();
        if (response.ok) {
          return json;
        }
      })
      .then(data => {
        data.routes[0].geometry.coordinates =
          data.routes[0].geometry.coordinates.map(coord => [coord[1], coord[0]]);
        return data;
      });
  }
}