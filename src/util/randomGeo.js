const METRES_TO_DEGREE = 111300,
  EARTH_RADIUS = 6371000;

export default function(center, radius) {
  const y0 = center.latitude,
    x0 = center.longitude,
    rd = radius / METRES_TO_DEGREE,

    u = Math.random(),
    v = Math.random(),
    w = rd * Math.sqrt(u),
    t = 2 * Math.PI * v,
    x = w * Math.cos(t),
    y = w * Math.sin(t),

    lat = y + y0,
    lon = x + x0;

  return {
    latitude: lat,
    longitude: lon,
    distance: distance(center.latitude, center.longitude, lat, lon).toFixed(2)
  };
}

function distance(lat1, lon1, lat2, lon2) {
  const angle = 0.5 - Math.cos((lat2 - lat1) * Math.PI / 180) / 2 + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * (1 - Math.cos((lon2 - lon1) * Math.PI / 180)) / 2;
  return EARTH_RADIUS * 2 * Math.asin(Math.sqrt(angle));
}